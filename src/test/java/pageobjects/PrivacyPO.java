package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 05/09/18
 * @project connecPath
 */
public class PrivacyPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected PrivacyPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=' Delete Account ']")
    AndroidElement deleteAccountOption;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Delete Your Account']")
    AndroidElement deleteYourAccountOption;


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Yes, I want to delete my account']")
    AndroidElement confirmDeleteMyAccountButton;

    public CommonPO tapOnDeleteAccountOption() {
        deleteAccountOption.click();
        waitUtils.waitForElementToBeVisible(deleteYourAccountOption, driver);
        deleteYourAccountOption.click();
        waitUtils.waitForElementToBeVisible(confirmDeleteMyAccountButton, driver);
        confirmDeleteMyAccountButton.click();
        return new CommonPO(driver);
    }

}
