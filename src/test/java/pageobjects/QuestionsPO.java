package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 06/09/18
 * @project connecPath
 */
public class QuestionsPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected QuestionsPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Write your question ...']")
    AndroidElement writeYourQuestionsEditText;

    public void setQuestion(String text){
        writeYourQuestionsEditText.sendKeys(text);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Post']")
    AndroidElement postButton;

    public ForumPO tapOnPostButton(){
        postButton.click();
        return new ForumPO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='OTHERS']")
    AndroidElement othersOption;

    public void tapOnOthersOption(){
        othersOption.click();
    }


}
