package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class SwipeCardsPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected SwipeCardsPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Skip']")
    AndroidElement skipButton;

    public AndroidElement getSkipButton() {
        return skipButton;
    }

    public CommonPO tapOnSkipButton() {
        getSkipButton().click();
        return new CommonPO(driver);
    }

    public boolean isSwipeCardsScreenDisplayed() {
        return getSkipButton().isDisplayed();
    }

}
