package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class ProfilePO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected ProfilePO(AppiumDriver driver) {
        super(driver);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Sign Up']")
    AndroidElement signUpButton;

    public AndroidElement getSignUpButton() {
        return signUpButton;
    }

    public SelectYourRolePO tapOnSignUpButton() {
        getSignUpButton().click();
        return new SelectYourRolePO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Log In']")
    AndroidElement logInButton;

    public LoginPO tapOnLogInButton() {
        logInButton.click();
        return new LoginPO(driver);
    }


    public boolean isProfileScreenDisplayed() {
        return getSignUpButton().isDisplayed();
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text=' Log Out ']")
    AndroidElement logoutButton;

    public CommonPO tapOnLogoutButton() {
        AppiumUtils.scrollDownToElement(logoutButton, driver);
        logoutButton.click();
        CommonPO commonPO = new CommonPO(driver);
        commonPO.waitTillDashboardIsDisplayed();
        return commonPO;
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=' Privacy ']")
    AndroidElement privacyButton;

    public PrivacyPO tapOnPrivacyOption() {
        AppiumUtils.scrollDownToElement(privacyButton, driver);
        privacyButton.click();
        return new PrivacyPO(driver);
    }


    @AndroidFindBy(xpath = "//android.view.ViewGroup[@index=\"3\"]")
    List<AndroidElement> settingsButton;

    public EditProfilePO tapOnSettingsButton() {
        //waitUtils.staticWait(5000);
        settingsButton.get(0).click();
        settingsButton.get(0).click();
        return new EditProfilePO(driver);
    }


}
