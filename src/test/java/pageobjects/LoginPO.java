package pageobjects;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 05/09/18
 * @project connecPath
 */
public class LoginPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected LoginPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> textField;

    public void setEmail(String email) {
        textField.get(0).sendKeys(email);
    }

    public void setPassword(String password) {
        textField.get(1).sendKeys(password);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Log In']")
    List<AndroidElement> logInButton;

    public CommonPO tapOnLoginButton() {
        System.out.println(logInButton.get(1).getCoordinates());
        System.out.println(logInButton.get(1).getLocation());
        AppiumUtils.clickOnPoint(logInButton.get(1).getLocation().getX() + 50, logInButton.get(1).getLocation().getY() + 30, driver);
        return new CommonPO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView")
    List<AndroidElement> errorTextView;

    public boolean isLoginButtonDisplayed() {
        return errorTextView.get(3).isDisplayed();
    }
}
