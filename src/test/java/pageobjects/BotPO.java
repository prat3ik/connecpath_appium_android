package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class BotPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    public BotPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(className = "android.widget.EditText")
    AndroidElement askAQuestionEditText;

    public boolean isBotScreenIsDisplayed() {
        return askAQuestionEditText.isDisplayed();
    }

}
