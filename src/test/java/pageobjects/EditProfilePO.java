package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 07/09/18
 * @project connecPath
 */
public class EditProfilePO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected EditProfilePO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText")
    List<AndroidElement> editText;

    public String getFirstName() {
        return editText.get(0).getText();
    }

    public void setFirstName(String firstName) {
        editText.get(0).clear();
        editText.get(0).sendKeys(firstName);
    }

    public String getLastName() {
        return editText.get(1).getText();
    }

    public void setLastName(String lastName) {
        editText.get(1).clear();
        editText.get(1).sendKeys(lastName);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Save']")
    AndroidElement saveButton;

    public ProfilePO tapOnSaveButton() {
        saveButton.click();
        return new ProfilePO(driver);
    }
}
