package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import utils.AppiumUtils;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class ForumPO extends BasePO {
    private final static String postText = "";

    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected ForumPO(AppiumDriver driver) {
        super(driver);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='POST A QUESTION']")
    AndroidElement postAQuestionTextView;

    public AndroidElement getPostAQuestionTextView() {
        return postAQuestionTextView;
    }

    public QuestionsPO tapOnPostAQuestionTextView() {
        System.out.println(getPostAQuestionTextView());
        getPostAQuestionTextView().click();
        return new QuestionsPO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='" + postText + "']")
    AndroidElement postTextView;

    public boolean isPostedTextVisible() {

        return AppiumUtils.isElementDisplayed(postTextView);
    }

    public boolean isForumScreenDisplayed() {
        return getPostAQuestionTextView().isDisplayed();
    }

    //"Prateik (11th)  "
    @AndroidFindBy(xpath = "//android.widget.TextView")
    List<AndroidElement> firstPostTextView;


    public String getRecentPostText(){
        for(int i=0;i<firstPostTextView.size();i++) {
            System.out.println(i);
            System.out.println("PPPPP:" + firstPostTextView.get(i).getText());
        }
        return firstPostTextView.get(6).getText();
    }
}
