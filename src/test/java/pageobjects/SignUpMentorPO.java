package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class SignUpMentorPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected SignUpMentorPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter First Name']")
    AndroidElement firstNameEditText;

    public void setFirsNameEditText(String firstName) {
        firstNameEditText.sendKeys(firstName);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter Last Name']")
    AndroidElement lastNameEditText;

    public void setLastNameEditText(String lastName) {
        lastNameEditText.sendKeys(lastName);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter School Email']")
    AndroidElement enterSchoolEmailEditText;

    public void setEnterSchoolEmailEditText(String lastName) {
        enterSchoolEmailEditText.sendKeys(lastName);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='At least 8 characters']")
    AndroidElement passwordEditText;

    public void setPasswordEditText(String lastName) {
        passwordEditText.sendKeys(lastName);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Re-type Password']")
    AndroidElement reTypePasswordEditText;

    public void setReTypePasswordEditText(String reTypePassword) {
        reTypePasswordEditText.sendKeys(reTypePassword);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Get Started']")
    AndroidElement getStartedButton;

    public SwipeCardsPO tapOnGetStartedButton(){
        getStartedButton.click();
        return new SwipeCardsPO(driver);
    }

}
