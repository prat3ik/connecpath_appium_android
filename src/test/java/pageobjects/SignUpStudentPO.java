package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.Dimension;
import utils.AppiumUtils;

import java.util.List;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class SignUpStudentPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected SignUpStudentPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Graduation Year']")
    List<AndroidElement> graduationYearTextField;

    protected void tapOnGraduationYearTextField(){
        System.out.println(graduationYearTextField.size());
        graduationYearTextField.get(1).click();
        waitUtils.staticWait(2000);

    }

    protected void tapOnDoneButton(){
        Dimension size = driver.manage().window().getSize();
        int width = size.getWidth();
        int height = size.getHeight();
        System.out.println(width);
        System.out.println(height);

        System.out.println(height*0.69);
        System.out.println(width*0.93);

        AppiumUtils.clickOnPoint((int) (width * 0.93), (int) (height * 0.69), driver);
    }

    public void setgraduationYear(){
        tapOnGraduationYearTextField();
        tapOnDoneButton();
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter First Name']")
    AndroidElement firstNameTextField;

    public void setFirstNameTextField(String firstName){
        firstNameTextField.sendKeys(firstName);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter Last Name']")
    AndroidElement lastNameTextField;

    public void setLastNameTextField(String lastName){
        lastNameTextField.sendKeys(lastName);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Enter Email']")
    AndroidElement emailTextField;

    public void setEmailTextField(String email){
        emailTextField.sendKeys(email);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='At least 8 characters']")
    AndroidElement passwordTextField;

    public void setPasswordTextField(String password){
        passwordTextField.sendKeys(password);
    }

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Re-type Password']")
    AndroidElement retypePasswordTextField;

    public void setRetypePasswordTextField(String password){
        retypePasswordTextField.sendKeys(password);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Get Started']")
    AndroidElement getStartedButton;

    public SwipeCardsPO tapOnGetStartedButton(){
        getStartedButton.click();
        return new SwipeCardsPO(driver);
    }

}
