package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class CommonPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    public CommonPO(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='MENTORS']")
    AndroidElement mentorsButton;

    public boolean isMentorsButtonDisplayed() {
        waitUtils.waitForElementToBeVisible(mentorsButton, driver);
        return mentorsButton.isDisplayed();
    }

    public MentorsPO tapOnMentorsButton() {
        mentorsButton.click();
        return new MentorsPO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FORUM']")
    AndroidElement forumButton;

    public ForumPO tapOnForumButton() {
        forumButton.click();
        return new ForumPO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='BOT']")
    AndroidElement botButton;

    public BotPO tapOnBotButton() {
        botButton.click();
        return new BotPO(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='PROFILE']")
    AndroidElement profileButton;

    public ProfilePO tapOnProfileButton() {
        profileButton.click();
        return new ProfilePO(driver);
    }

    public void waitTillDashboardIsDisplayed() {
        waitUtils.staticWait(4000);
        waitUtils.waitForElementToBeVisible(botButton, driver);
    }
}
