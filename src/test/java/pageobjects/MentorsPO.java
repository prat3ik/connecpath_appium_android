package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class MentorsPO extends BasePO {
    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this postTextView in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected MentorsPO(AppiumDriver driver) {
        super(driver);
    }


    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Start 10 min Session']")
    AndroidElement start10MinSessionButton;

    public AndroidElement getStart10MinSessionButton() {
        return start10MinSessionButton;
    }

    public void tapOnStart10MinSessionButton() {
        getStart10MinSessionButton().click();
    }

    public boolean isMentorScreenDisplayed() {
        return getStart10MinSessionButton().isDisplayed();
    }
}
