/*
 * (C) Copyright 2018 by Pratik Patel (https://github.com/prat3ik/)
 */
package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobjects.*;
import utils.PropertyUtils;

/**
 * This is the Test Cases Class for Student workflow, All the test cases are defined in this.
 * Year: 2018-19
 *
 * @author Prat3ik on 04/09/18
 * @project connecPath
 */
public class StudentTestCases extends BaseTest {

    int COMMENT_ICON_X = PropertyUtils.getIntegerProperty("Pixel.comment.icon_x", 980);

    @BeforeTest
    @Override
    public void setUpPage() {

    }

    @Test
    public void verifyAppIsBeingLaunched() {
        Assert.assertTrue(new BotPO(driver).isBotScreenIsDisplayed(), "App is not launched correctly");
    }

    @Test
    public void verifyUserCanSeeAllTheTabs() {
        CommonPO commonPO = new CommonPO(driver);
        MentorsPO mentorsPO = commonPO.tapOnMentorsButton();
        Assert.assertTrue(mentorsPO.isMentorScreenDisplayed(), "Mentors screen didn't display!");

        ForumPO forumPO = commonPO.tapOnForumButton();
        Assert.assertTrue(forumPO.isForumScreenDisplayed(), "Forum screen didn't display!");

        BotPO botPO = commonPO.tapOnBotButton();
        Assert.assertTrue(botPO.isBotScreenIsDisplayed(), "Bot screen didn't display!");

        ProfilePO profilePO = commonPO.tapOnProfileButton();
        Assert.assertTrue(profilePO.isProfileScreenDisplayed(), "Profile screen didn't display!");
    }

    @Test
    public void verifyStudentCanSignUpLoginAndDeleteTheAccount() {
        String firstName = "Pratik";
        String lastName = "Patel";

        String email = "pratikquora+08@gmail.com";
        String password = "test12345";

//        String email1= "pratikquora+9@gmail.com";
//        String password1 = "qwertyui";

        CommonPO commonPO = new CommonPO(driver);
        ProfilePO profilePO = commonPO.tapOnProfileButton();

        SelectYourRolePO selectYourRolePO = profilePO.tapOnSignUpButton();
        SignUpStudentPO signUpStudentPO = selectYourRolePO.tapOnHighSchoolButton();
        signUpStudentPO.setgraduationYear();
        signUpStudentPO.setFirstNameTextField(firstName);
        signUpStudentPO.setLastNameTextField(lastName);
        signUpStudentPO.setEmailTextField(email);
        signUpStudentPO.setPasswordTextField(password);
        signUpStudentPO.setRetypePasswordTextField(password);
        SwipeCardsPO swipeCardsPO = signUpStudentPO.tapOnGetStartedButton();
        Assert.assertTrue(swipeCardsPO.isSwipeCardsScreenDisplayed(), "Registration of Student has failed!");
        commonPO = swipeCardsPO.tapOnSkipButton();
        Assert.assertTrue(commonPO.isMentorsButtonDisplayed(), "Mentors screen is not displyed");
        profilePO = commonPO.tapOnProfileButton();
        commonPO = profilePO.tapOnLogoutButton();

        profilePO = commonPO.tapOnProfileButton();
        LoginPO loginPO = profilePO.tapOnLogInButton();
        loginPO.setEmail(email);
        loginPO.setPassword(password);
        commonPO = loginPO.tapOnLoginButton();
        profilePO = commonPO.tapOnProfileButton();

        PrivacyPO privacyPO = profilePO.tapOnPrivacyOption();
        commonPO = privacyPO.tapOnDeleteAccountOption();
        profilePO = commonPO.tapOnProfileButton();

        Assert.assertTrue(profilePO.isProfileScreenDisplayed(), "Account is still logged in even after it's deleted!");
        loginPO = profilePO.tapOnLogInButton();
        loginPO.setEmail(email);
        loginPO.setPassword(password);
        loginPO.tapOnLoginButton();
        Assert.assertTrue(loginPO.isLoginButtonDisplayed(), "Error Message did not display!");
    }

    @Test
    public void verifyUserCanPostQuestion() {
        String email = "pratikquora+000@gmail.com";
        String password = "qwertyui";
        String postText = "Test"+Math.random();

        CommonPO commonPO = new CommonPO(driver);
        ProfilePO profilePO = commonPO.tapOnProfileButton();
        LoginPO loginPO = profilePO.tapOnLogInButton();
        loginPO.setEmail(email);
        loginPO.setPassword(password);
        commonPO = loginPO.tapOnLoginButton();
        ForumPO forumPO = commonPO.tapOnForumButton();

        QuestionsPO questionsPO = forumPO.tapOnPostAQuestionTextView();
        questionsPO.tapOnOthersOption();
        questionsPO.setQuestion(postText);
        forumPO = questionsPO.tapOnPostButton();

        Assert.assertEquals(forumPO.getRecentPostText(), postText, "Posted Question did not displayed!");

        profilePO = commonPO.tapOnProfileButton();
        commonPO = profilePO.tapOnLogoutButton();
        profilePO = commonPO.tapOnProfileButton();

        Assert.assertTrue(profilePO.isProfileScreenDisplayed(), "Log out is not successful");
    }

    @Test
    public void verifyStudentCanEditTheProfile() {
        String email = "pratikquora+000@gmail.com";
        String password = "qwertyui";
        String firstName = "Prateik";
        String lastName = "Chanchpara";

        CommonPO commonPO = new CommonPO(driver);
        ProfilePO profilePO = commonPO.tapOnProfileButton();
        LoginPO loginPO = profilePO.tapOnLogInButton();
        loginPO.setEmail(email);
        loginPO.setPassword(password);
        commonPO = loginPO.tapOnLoginButton();

        profilePO = commonPO.tapOnProfileButton();
        EditProfilePO editProfilePO = profilePO.tapOnSettingsButton();
        editProfilePO.setFirstName(firstName);
        editProfilePO.setLastName(lastName);
        profilePO = editProfilePO.tapOnSaveButton();

        editProfilePO = profilePO.tapOnSettingsButton();
        String newFirstName = editProfilePO.getFirstName();
        String newLastName = editProfilePO.getLastName();

        Assert.assertEquals(newFirstName, firstName, "First Name didn't match.");
        Assert.assertEquals(newLastName, lastName, "Last Name didn't match.");

        profilePO = editProfilePO.tapOnSaveButton();
        commonPO = profilePO.tapOnLogoutButton();
        profilePO = commonPO.tapOnProfileButton();

        Assert.assertTrue(profilePO.isProfileScreenDisplayed(), "Log out is not successful");
    }


}
