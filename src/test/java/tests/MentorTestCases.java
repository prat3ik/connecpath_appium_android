package tests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobjects.*;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 08/08/18
 * @project connecPath
 */
public class MentorTestCases extends BaseTest {
    @BeforeTest
    @Override
    public void setUpPage() {

    }

    @Test
    public void verifyMentorCanSignUp(){
        String firstName = "Pratik";
        String lastName = "Patel";
        String schoolEmail = "pratikquora+"+(int)Math.random()*100+"@edu.com";
        String password = "qwertyui";

        CommonPO commonPO = new CommonPO(driver);
        ProfilePO profilePO = commonPO.tapOnProfileButton();
        SelectYourRolePO selectYourRolePO = profilePO.tapOnSignUpButton();
        SignUpMentorPO signUpMentorPO = selectYourRolePO.tapOnCollegeStudentButton();
        signUpMentorPO.setFirsNameEditText(firstName);
        signUpMentorPO.setLastNameEditText(lastName);
        signUpMentorPO.setEnterSchoolEmailEditText(schoolEmail);
        signUpMentorPO.setPasswordEditText(password);
        signUpMentorPO.setReTypePasswordEditText(password);
        SwipeCardsPO swipeCardsPO = signUpMentorPO.tapOnGetStartedButton();


    }
}
